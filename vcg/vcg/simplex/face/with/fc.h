#ifndef __VCGLIB_FACE_FC_TYPE
#define __VCGLIB_FACE_FC_TYPE

#define FACE_TYPE FaceFC 

#define __VCGLIB_FACE_FC

#include <vcg/simplex/face/base.h> 

#undef FACE_TYPE 

#undef __VCGLIB_FACE_FC

#endif
