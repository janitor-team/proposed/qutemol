#ifndef __VCGLIB_FACE_RTFMFN_TYPE
#define __VCGLIB_FACE_RTFMFN_TYPE

#define FACE_TYPE FaceRTFCFMFN

#define __VCGLIB_FACE_RT
#define __VCGLIB_FACE_FC
#define __VCGLIB_FACE_FM
#define __VCGLIB_FACE_FN

#include <vcg/simplex/face/base.h> 

#undef FACE_TYPE 

#undef __VCGLIB_FACE_RT
#undef __VCGLIB_FACE_FC
#undef __VCGLIB_FACE_FM
#undef __VCGLIB_FACE_FN

#endif
