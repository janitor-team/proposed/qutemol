#ifndef __VCGLIB_FACE_AFAV_TYPE
#define __VCGLIB_FACE_AFAV_TYPE

#define FACE_TYPE FaceAFAV 

#define __VCGLIB_FACE_AV
#define __VCGLIB_FACE_AF

#include <vcg/simplex/face/base.h> 

#undef FACE_TYPE 

#undef __VCGLIB_FACE_AF
#undef __VCGLIB_FACE_AV

#endif
