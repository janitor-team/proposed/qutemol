#ifndef __VCGLIB_FACE_FCFN_TYPE
#define __VCGLIB_FACE_FCFN_TYPE

#define FACE_TYPE FaceFCFN 

#define __VCGLIB_FACE_FC
#define __VCGLIB_FACE_FN

#include <vcg/simplex/face/base.h> 

#undef FACE_TYPE 

#define __VCGLIB_FACE_FC
#define __VCGLIB_FACE_FN

#endif
