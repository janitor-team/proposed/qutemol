#ifndef __VCGLIB_FACE_FM_TYPE
#define __VCGLIB_FACE_FM_TYPE

#define FACE_TYPE FaceFM 

#define __VCGLIB_FACE_FM

#include <vcg/simplex/face/base.h> 

#undef FACE_TYPE 

#undef __VCGLIB_FACE_FM

#endif /* __VCGLIB_FACE_FM_TYPE */
