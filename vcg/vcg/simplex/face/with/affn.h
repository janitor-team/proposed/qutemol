#ifndef __VCGLIB_FACE_AFFN_TYPE
#define __VCGLIB_FACE_AFFN_TYPE

#define FACE_TYPE FaceAFFN

#define __VCGLIB_FACE_AF
#define __VCGLIB_FACE_FN


#include <vcg/simplex/face/base.h> 

#undef FACE_TYPE 

#undef __VCGLIB_FACE_AF
#undef __VCGLIB_FACE_FN

#endif