#ifndef __VCGLIB_FACE_AFAVFN_TYPE
#define __VCGLIB_FACE_AFAVFN_TYPE

#define FACE_TYPE FaceAFAVFN

#define __VCGLIB_FACE_AF
#define __VCGLIB_FACE_FN
#define __VCGLIB_FACE_AV

#include <vcg/simplex/face/base.h> 

#undef FACE_TYPE 

#undef __VCGLIB_FACE_AF
#undef __VCGLIB_FACE_FN
#undef __VCGLIB_FACE_AV

#endif