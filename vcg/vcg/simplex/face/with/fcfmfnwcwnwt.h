#ifndef __VCGLIB_FACE_FCFMFNWCWNWT_TYPE
#define __VCGLIB_FACE_FCFMFNWCWNWT_TYPE

#define FACE_TYPE FaceFCFMFNWCWNWT 

#define __VCGLIB_FACE_FC
#define __VCGLIB_FACE_FM
#define __VCGLIB_FACE_FN
#define __VCGLIB_FACE_WC
#define __VCGLIB_FACE_WN
#define __VCGLIB_FACE_WT

#include <vcg/simplex/face/base.h> 

#undef __VCGLIB_FACE_FC
#undef __VCGLIB_FACE_FM
#undef __VCGLIB_FACE_FN
#undef __VCGLIB_FACE_WC
#undef __VCGLIB_FACE_WN
#undef __VCGLIB_FACE_WT

#undef FACE_TYPE 

#endif /* __VCGLIB_FACE_FCFMFNWCWNWT_TYPE */
