#ifndef __VCGLIB_TETRA_TQ_TYPE
#define __VCGLIB_TETRA_TQ_TYPE

#define TETRA_TYPE TetraTQ 

#define __VCGLIB_TETRA_TQ

#include <vcg/simplex/tetrahedron/base.h> 

#undef TETRA_TYPE 

#undef __VCGLIB_TETRA_TQ

#endif
