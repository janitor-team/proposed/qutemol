#ifndef __VCGLIB_TETRA_ATAVTQ_TYPE
#define __VCGLIB_TETRA_ATAVTQ_TYPE

#define TETRA_TYPE TetraATAVTQ 

#define __VCGLIB_TETRA_AT
#define __VCGLIB_TETRA_AV
#define __VCGLIB_TETRA_TQ
#include <vcg/simplex/tetrahedron/base.h> 

#undef TETRA_TYPE 

#undef __VCGLIB_TETRA_AT
#undef __VCGLIB_TETRA_AV
#undef __VCGLIB_TETRA_TQ

#endif
