#ifndef __VCGLIB_TETRA_ATAV_TYPE
#define __VCGLIB_TETRA_ATAV_TYPE

#define TETRA_TYPE TetraATAV

#define __VCGLIB_TETRA_AT
#define __VCGLIB_TETRA_AV
#include <vcg/simplex/tetrahedron/base.h> 

#undef TETRA_TYPE 

#undef __VCGLIB_TETRA_AT
#undef __VCGLIB_TETRA_AV

#endif
